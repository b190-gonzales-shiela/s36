// contains all endpoints of our application

const express= require("express");

const router= express.Router();

const taskController= require("../controllers/taskController.js");
router.get("/",(req,res) => {

	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
});

router.post("/",(req,res) =>{

	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
});

// delteing task
router.delete("/:id",(req,res) => {
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
});

// route updating task

router.put("/:id", (req,res)=> {
	taskController.updateTask(req.params.id,req.body).then(resultFromController => res.send(resultFromController));


});



// ACTIVTY


/*1. Create a route for getting a specific task.
2. Create a controller function for retrieving a specific task.
3. Return the result back to the client/Postman.
4. Process a GET request at the "/tasks/:id" route using postman to get a specific task.
5. Create a route for changing the status of a task to "complete".
6. Create a controller function for changing the status of a task to "complete".
7. Return the result back to the client/Postman.
8. Process a PUT request at the "/tasks/:id/complete" route using postman to update a task.
9. Create a git repository named S36.
10. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
11. Add the link in Boodle.
*/

router.get("/tasks/:id",(req,res) => {
	
	taskController.getTask(req.params.id,req.body).then(resultFromController => res.send(resultFromController));
});

router.put("/tasks/:id/complete", (req,res) =>{

	taskController.completeTask(req.params.id,req.body).then(resultFromController => res.send(resultFromController));
});


























module.exports = router;
