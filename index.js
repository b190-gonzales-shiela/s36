const express= require("express");

const mongoose= require("mongoose");

const app =express ();

const port = 3000;

const taskRoutes = require("./routes/taskRoutes.js");


mongoose.connect("mongodb+srv://shielamaemgonzales:gonzales@wdc028-course-booking.b55st6r.mongodb.net/b190-to-do?retryWrites=true&w=majority",


	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}

);

let db = mongoose.connection;


db.on("error",console.error.bind(console,"connection error"));



db.once("open",()=>console.log("we're connected to the database"));


app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use("/tasks", taskRoutes);
app.use("/tasks/:id/complete", taskRoutes);

app.listen(port,() => console.log(`Server running at port: ${port}`));