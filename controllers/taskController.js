// all operations will be placed in this file

const Task=require("../models/task.js");


module.exports.getAllTasks =()=>{

	return Task.find({ }).then(result =>{
		return result;
	});
};


module.exports.createTask=(requestBody) => {

	let newTask= new Task ({

		name: requestBody.name
	});

	return newTask.save().then((task, error) =>{

		if (error){
			console.log(error);
			return false;

		} else{
			return task;
		}
	});

};


module.exports.deleteTask=(taskId) => {

	return Task.findByIdAndRemove(taskId).then((result,error)=>{
			if (error){
				console.log(error);
				return false;

			} else {
				return result;

			}
		});
};

// UPDATING TASK
module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		}else{
			result.name = newContent.name;
			return result.save().then((updatedTask, error)=>{
				if (error){
					console.log(error);
					return false
				}else{
					return updatedTask;
				};
			});
		};
	});
};


//ACTIVITY

module.exports.getTask = (taskId, newTask) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		}else {
			result.name = newTask.name;
			return result.save().then((getTask, error)=>{
				if (error){
					console.log(error);
					return false
				}else{
					return newTask;
				};
			});
		};
	});
};

module.exports.completedTask = (taskId, newCompletedTask) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		}else{
			result.name = newCompletedTask.name;
			return result.save().then((completedTask, error)=>{
				if (error){
					console.log(error);
					return false
				}else{
					return updatedTask;
				};
			});
		};
	});
};